contentitemexportablefields:
  contentitemfields:
    fromServerVersion: 5.0.0
    itemVersion: 4.2.2
    packID: CortexXDR
    packPropagationLabels:
    - all
    propagationLabels: []
    toServerVersion: ""
description: "This playbook is used to loop over every alert in a Cortex XDR incident.
  \nSupported alert categories:\n- Malware\n- Port Scan"
id: Cortex XDR Alerts Handling
inputs:
- description: Incident ID.
  key: incident_id
  playbookInputQuery: null
  required: false
  value:
    complex:
      accessor: Incident.incident_id
      root: PaloAltoNetworksXDR
- description: Alert ID.
  key: alert_id
  playbookInputQuery: null
  required: false
  value:
    complex:
      accessor: Incident.alerts.alert_id
      root: PaloAltoNetworksXDR
name: Cortex XDR Alerts Handling
outputs:
- contextPath: PaloAltoNetworksXDR.Incident.incident_id
  description: Unique ID assigned to each returned incident.
- contextPath: PaloAltoNetworksXDR.Incident.description
  description: Dynamic calculated description of the incident.
- contextPath: PaloAltoNetworksXDR.Incident.alerts.alert_id
  description: Unique ID for each alert.
- contextPath: PaloAltoNetworksXDR.Incident.alerts.severity
  description: Severity of the alert.,"low","medium","high"""
- contextPath: PaloAltoNetworksXDR.Incident.alerts.name
  description: Calculated name of the alert.
- contextPath: PaloAltoNetworksXDR.Incident.alerts.category
  description: Category of the alert, for example, Spyware Detected via Anti-Spyware
    profile.
- contextPath: PaloAltoNetworksXDR.Incident.alerts.host_ip
  description: Host IP involved in the alert.
- contextPath: PaloAltoNetworksXDR.Incident.alerts.host_name
  description: Host name involved in the alert.
- contextPath: PaloAltoNetworksXDR.Incident.alerts.user_name
  description: User name involved with the alert.
- contextPath: PaloAltoNetworksXDR.Incident.alerts.event_type
  description: Event type "Process Execution","Network Event","File Event","Registry
    Event","Injection Event","Load Image Event","Windows Event Log"
- contextPath: PaloAltoNetworksXDR.Incident.alerts.action
  description: The action that triggered the alert. "REPORTED", "BLOCKED", "POST_DETECTED",
    "SCANNED", "DOWNLOAD", "PROMPT_ALLOW", "PROMPT_BLOCK", "DETECTED", "BLOCKED_1",
    "BLOCKED_2", "BLOCKED_3", "BLOCKED_5", "BLOCKED_6", "BLOCKED_7", "BLOCKED_8",
    "BLOCKED_9", "BLOCKED_10", "BLOCKED_11", "BLOCKED_13", "BLOCKED_14", "BLOCKED_15",
    "BLOCKED_16", "BLOCKED_17", "BLOCKED_24", "BLOCKED_25", "DETECTED_0", "DETECTED_4",
    "DETECTED_18", "DETECTED_19", "DETECTED_20", "DETECTED_21", "DETECTED_22", "DETECTED_23"
- contextPath: PaloAltoNetworksXDR.Incident.alerts.action_pretty
  description: The action that triggered the alert "Detected (Reported)" "Prevented
    (Blocked)" "Detected (Post Detected)" "Detected (Scanned)" "Detected (Download)"
    "Detected (Prompt Allow)" "Prevented (Prompt Block)" "Detected" "Prevented (Denied
    The Session)" "Prevented (Dropped The Session)" "Prevented (Dropped The Session
    And Sent a TCP Reset)" "Prevented (Blocked The URL)" "Prevented (Blocked The IP)"
    "Prevented (Dropped The Packet)" "Prevented (Dropped All Packets)" "Prevented
    (Terminated The Session And Sent a TCP Reset To Both Sides Of The Connection)"
    "Prevented (Terminated The Session And Sent a TCP Reset To The Client)" "Prevented
    (Terminated The Session And Sent a TCP Reset To The Server)" "Prevented (Continue)"
    "Prevented (Block-Override)" "Prevented (Override-Lockout)" "Prevented (Override)"
    "Prevented (Random-Drop)" "Prevented (Silently Dropped The Session With An ICMP
    Unreachable Message To The Host Or Application)" "Prevented (Block)" "Detected
    (Allowed The Session)" "Detected (Raised An Alert)" "Detected (Syncookie Sent)"
    "Detected (Forward)" "Detected (Wildfire Upload Success)" "Detected (Wildfire
    Upload Failure)" "Detected (Wildfire Upload Skip)" "Detected (Sinkhole)"
- contextPath: PaloAltoNetworksXDR.Incident.alerts.actor_process_image_name
  description: Image name
- contextPath: PaloAltoNetworksXDR.Incident.alerts.actor_process_command_line
  description: Command line
- contextPath: PaloAltoNetworksXDR.Incident.alerts.actor_process_signature_status
  description: Signature status "Signed" "Invalid Signature" "Unsigned" "Revoked"
    "Signature Fail" "N/A" "Weak Hash"
- contextPath: PaloAltoNetworksXDR.Incident.alerts.actor_process_signature_vendor
  description: Singature vendor name
- contextPath: PaloAltoNetworksXDR.Incident.alerts.action_process_image_sha256
  description: Image SHA256
- contextPath: PaloAltoNetworksXDR.Incident.alerts.is_whitelisted
  description: Is whitelisted "Yes" "No"
- contextPath: PaloAltoNetworksXDR.Incident.network_artifacts.type
  description: Network artifact type "IP"
- contextPath: PaloAltoNetworksXDR.Incident.network_artifacts.network_domain
  description: The domain related to the artifact.
- contextPath: PaloAltoNetworksXDR.Incident.network_artifacts.network_country
  description: The country related to the artifact
- contextPath: PaloAltoNetworksXDR.Incident.network_artifacts.network_remote_ip
  description: The remote IP related to the artifact.
- contextPath: PaloAltoNetworksXDR.Incident.file_artifacts.file_signature_status
  description: Digital signature status of the file. "SIGNATURE_UNAVAILABLE" "SIGNATURE_SIGNED"
    "SIGNATURE_INVALID" "SIGNATURE_UNSIGNED" "SIGNATURE_WEAK_HASH"
- contextPath: PaloAltoNetworksXDR.Incident.file_artifacts.is_process
  description: Whether the file artifact is related to a process execution.
- contextPath: PaloAltoNetworksXDR.Incident.file_artifacts.file_name
  description: Name of the file.
- contextPath: PaloAltoNetworksXDR.Incident.file_artifacts.file_wildfire_verdict
  description: The file verdict, calculated by Wildfire. "BENIGN" "MALWARE" "GRAYWARE"
    "PHISING" "UNKNOWN"
- contextPath: PaloAltoNetworksXDR.Incident.file_artifacts.is_malicious
  description: Whether the artifact is malicious, decided by the Wildfire verdic
- contextPath: PaloAltoNetworksXDR.Incident.file_artifacts.type
  description: The artifact type "META" "GID" "CID" "HASH" "IP" "DOMAIN" "REGISTRY"
    "HOSTNAME"
- contextPath: PaloAltoNetworksXDR.Incident.file_artifacts.file_sha256
  description: SHA-256 hash of the file
- contextPath: PaloAltoNetworksXDR.Incident.file_artifacts.file_signature_vendor_name
  description: File signature vendor name
- contextPath: PortScan.BlockPorts
  description: Indicates whether there's a need to block the ports used for exploitation
    on the scanned host.
  type: unknown
- contextPath: PortScan.AttackerIPs
  description: Attacker IPs from the port scan alert.
  type: unknown
- contextPath: PortScan.AttackerHostnames
  description: Attacker hostnames from the port scan alert.
  type: unknown
- contextPath: PortScan.AttackerUsername
  description: Attacker username from the port scan alert.
  type: unknown
- contextPath: PortScan.FileArtifacts
  description: File artifacts from the port scan alert.
  type: unknown
- contextPath: PortScan.LateralMovementFirstDatetime
  description: Lateral Movement First Date time from the port scan alert.
  type: unknown
- contextPath: PortScan.PortScanFirstDatetime
  description: Port Scan First Date time
  type: unknown
starttaskid: "0"
system: true
tasks:
  "0":
    id: "0"
    ignoreworker: false
    isautoswitchedtoquietmode: false
    isoversize: false
    nexttasks:
      '#none#':
      - "6"
    note: false
    quietmode: 0
    separatecontext: false
    skipunavailable: false
    task:
      brand: ""
      id: 9c36d95d-b324-4c82-8a03-1994ede59fdf
      iscommand: false
      name: ""
      version: -1
    taskid: 9c36d95d-b324-4c82-8a03-1994ede59fdf
    timertriggers: []
    type: start
    view: |-
      {
        "position": {
          "x": 480,
          "y": 50
        }
      }
  "1":
    conditions:
    - condition:
      - - left:
            iscontext: true
            value:
              complex:
                accessor: Incident.alerts.category
                filters:
                - - left:
                      iscontext: true
                      value:
                        simple: PaloAltoNetworksXDR.Incident.alerts.alert_id
                    operator: isEqualString
                    right:
                      iscontext: true
                      value:
                        simple: inputs.alert_id
                root: PaloAltoNetworksXDR
          operator: isEqualString
          right:
            value:
              simple: Malware
      label: Malware
    - condition:
      - - left:
            iscontext: true
            value:
              complex:
                accessor: Incident.alerts.name
                filters:
                - - left:
                      iscontext: true
                      value:
                        simple: PaloAltoNetworksXDR.Incident.alerts.alert_id
                    operator: isEqualString
                    right:
                      iscontext: true
                      value:
                        simple: inputs.alert_id
                root: PaloAltoNetworksXDR
          operator: isEqualString
          right:
            value:
              simple: Port Scan
      label: Port Scan
    id: "1"
    ignoreworker: false
    isautoswitchedtoquietmode: false
    isoversize: false
    nexttasks:
      '#default#':
      - "7"
      Malware:
      - "2"
      Port Scan:
      - "8"
    note: false
    quietmode: 0
    separatecontext: false
    skipunavailable: false
    task:
      brand: ""
      description: Choose playbook to run by alert category.
      id: 7e32b542-4fd0-4a05-8f6a-914194fc2313
      iscommand: false
      name: Choose playbook by category
      type: condition
      version: -1
    taskid: 7e32b542-4fd0-4a05-8f6a-914194fc2313
    timertriggers: []
    type: condition
    view: |-
      {
        "position": {
          "x": 480,
          "y": 370
        }
      }
  "2":
    id: "2"
    ignoreworker: false
    isautoswitchedtoquietmode: false
    isoversize: false
    loop:
      exitCondition: ""
      iscommand: false
      max: 100
      wait: 1
    nexttasks:
      '#none#':
      - "5"
    note: false
    quietmode: 0
    scriptarguments:
      file_name:
        complex:
          accessor: Incident.file_artifacts.file_name
          filters:
          - - left:
                iscontext: true
                value:
                  simple: PaloAltoNetworksXDR.Incident.alerts.alert_id
              operator: isEqualString
              right:
                iscontext: true
                value:
                  simple: inputs.alert_id
          root: PaloAltoNetworksXDR
      file_sha256:
        complex:
          accessor: Incident.file_artifacts.file_sha256
          filters:
          - - left:
                iscontext: true
                value:
                  simple: PaloAltoNetworksXDR.Incident.alerts.alert_id
              operator: isEqualString
              right:
                iscontext: true
                value:
                  simple: inputs.alert_id
          root: PaloAltoNetworksXDR
      host_ip:
        complex:
          accessor: host_ip_list
          filters:
          - - left:
                iscontext: true
                value:
                  simple: PaloAltoNetworksXDR.Incident.alerts.alert_id
              operator: isEqualString
              right:
                iscontext: true
                value:
                  simple: inputs.alert_id
          root: PaloAltoNetworksXDR.Incident.alerts
          transformers:
          - operator: uniq
      xdr_alert_id:
        complex:
          root: inputs.alert_id
    separatecontext: false
    skipunavailable: false
    task:
      brand: ""
      description: "This playbook is triggered by a Palo Alto Networks Cortex threat
        alert, generated by Traps.  The playbook performs host enrichment for the
        source host with Palo Alto Networks Traps, enriches information for the suspicious
        file with Palo Alto Networks Minemeld and AutoFocus, and automatically performs
        file detonation for the extracted file. It then performs IOC enrichment with
        Minemeld for all related IOCs, and calculates the incident severity based
        on all the findings. In addition, it detonates the file for the full analysis
        report. \nThe analyst can perform a manual memory dump for the suspected endpoint
        based on the incident’s severity, and choose to isolate the source endpoint
        with Traps.\nHunting tasks to find more endpoints that are infected are performed
        automatically based on a playbook input, and after all infected endpoints
        are found, remediation for all malicious IOCs is performed, including file
        quarantine, and IP and URLs blocking with Palo Alto Networks FireWall components
        such as Dynamic Address Groups and Custom URL Categories.\nAfter the investigation
        review, the incident is automatically closed."
      id: 2dabd406-b818-4523-82c8-b31a6808d5fd
      iscommand: false
      name: Cortex XDR - Malware Investigation
      playbookId: Cortex XDR - Malware Investigation
      type: playbook
      version: -1
    taskid: 2dabd406-b818-4523-82c8-b31a6808d5fd
    timertriggers: []
    type: playbook
    view: |-
      {
        "position": {
          "x": 50,
          "y": 545
        }
      }
  "5":
    id: "5"
    ignoreworker: false
    isautoswitchedtoquietmode: false
    isoversize: false
    note: false
    quietmode: 0
    separatecontext: false
    skipunavailable: false
    task:
      brand: ""
      id: 3ee49877-ed34-469e-8f5b-73536d3d40bd
      iscommand: false
      name: Done
      type: title
      version: -1
    taskid: 3ee49877-ed34-469e-8f5b-73536d3d40bd
    timertriggers: []
    type: title
    view: |-
      {
        "position": {
          "x": 480,
          "y": 720
        }
      }
  "6":
    id: "6"
    ignoreworker: false
    isautoswitchedtoquietmode: false
    isoversize: false
    nexttasks:
      '#none#':
      - "1"
    note: false
    quietmode: 0
    scriptarguments:
      incident_id:
        complex:
          root: inputs.incident_id
    separatecontext: false
    skipunavailable: false
    task:
      brand: Cortex XDR - IR
      description: Returns additional data for the specified incident, for example,
        related alerts, file artifacts, network artifacts, and so on.
      id: 12258730-025a-4931-8da9-9f68bfb6a32c
      iscommand: true
      name: Cortex XDR - get incident extra data
      script: Cortex XDR - IR|||xdr-get-incident-extra-data
      type: regular
      version: -1
    taskid: 12258730-025a-4931-8da9-9f68bfb6a32c
    timertriggers: []
    type: regular
    view: |-
      {
        "position": {
          "x": 480,
          "y": 195
        }
      }
  "7":
    id: "7"
    ignoreworker: false
    isautoswitchedtoquietmode: false
    isoversize: false
    nexttasks:
      '#none#':
      - "5"
    note: false
    quietmode: 0
    separatecontext: false
    skipunavailable: false
    task:
      brand: ""
      id: 73393504-1664-4f81-8baf-f5e29f222cea
      iscommand: false
      name: Other alert category
      type: title
      version: -1
    taskid: 73393504-1664-4f81-8baf-f5e29f222cea
    timertriggers: []
    type: title
    view: |-
      {
        "position": {
          "x": 920,
          "y": 545
        }
      }
  "8":
    id: "8"
    ignoreworker: false
    isautoswitchedtoquietmode: false
    isoversize: false
    loop:
      exitCondition: ""
      iscommand: false
      max: 100
      wait: 1
    nexttasks:
      '#none#':
      - "5"
    note: false
    quietmode: 0
    scriptarguments:
      BlockAttackerIP:
        simple: "False"
      OnCall:
        simple: "false"
      xdr_alert_id:
        complex:
          root: inputs.alert_id
    separatecontext: true
    skipunavailable: false
    task:
      brand: ""
      id: dbf4c20e-2308-41d5-88df-477133c8e571
      iscommand: false
      name: Cortex XDR - Port Scan - Adjusted
      playbookId: Cortex XDR - Port Scan - Adjusted
      type: playbook
      version: -1
    taskid: dbf4c20e-2308-41d5-88df-477133c8e571
    timertriggers: []
    type: playbook
    view: |-
      {
        "position": {
          "x": 480,
          "y": 545
        }
      }
version: -1
view: |-
  {
    "linkLabelsPosition": {},
    "paper": {
      "dimensions": {
        "height": 735,
        "width": 1250,
        "x": 50,
        "y": 50
      }
    }
  }
